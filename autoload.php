<?php

spl_autoload_register(
    function ($class_name) {
        $app_namespace = 'App';
        $app_path = __DIR__;
        $lib_path = $app_path . '/lib';

        if (str_starts_with($class_name, 'Minz\\')) {
            include $lib_path . '/Minz/autoload.php';
        } elseif (str_starts_with($class_name, 'SpiderBits\\')) {
            include $lib_path . '/SpiderBits/autoload.php';
        } elseif (str_starts_with($class_name, "{$app_namespace}\\")) {
            $class_name = substr($class_name, strlen($app_namespace) + 1);
            $class_path = str_replace('\\', '/', $class_name) . '.php';
            include $app_path . '/src/' . $class_path;
        }
    }
);
