<?php

$app_path = realpath(__DIR__ . '/..');

include $app_path . '/autoload.php';

\Minz\Configuration::load('dotenv', $app_path);
\Minz\Environment::initialize();
\Minz\Environment::startSession();

$request_method = strtolower($_SERVER['REQUEST_METHOD']);
$http_method = $request_method === 'head' ? 'get' : $request_method;
$http_uri = $_SERVER['REQUEST_URI'];
$http_parameters = array_merge($_GET, $_POST);
$http_headers = array_merge($_SERVER, [
    'COOKIE' => $_COOKIE,
]);

$request = new \Minz\Request($http_method, $http_uri, $http_parameters, $http_headers);

$application = new \App\Application();
$response = $application->run($request);

http_response_code($response->code());

foreach ($response->cookies() as $cookie) {
    setcookie($cookie['name'], $cookie['value'], $cookie['options']);
}

foreach ($response->headers() as $header) {
    header($header);
}

if ($request_method !== 'head') {
    echo $response->render();
}
