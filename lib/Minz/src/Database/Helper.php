<?php

namespace Minz\Database;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class Helper
{
    public static function buildWhere(array $criteria): array
    {
        $params = [];
        $where_statements = [];

        foreach ($criteria as $property => $condition) {
            if (is_array($condition)) {
                $params = array_merge($params, $condition);
                $question_marks = array_fill(0, count($condition), '?');
                $in_statement = implode(',', $question_marks);
                $where_statements[] = "{$property} IN ({$in_statement})";

            } elseif ($condition === null) {
                $where_statements[] = "{$property} IS NULL";

            } else {
                $params[] = $condition;
                $where_statements[] = "{$property} = ?";
            }
        }

        $sql_where = implode(' AND ', $where_statements);
        return [$sql_where, $params];
    }

    public static function convertValuesToDatabase(string $class_name, array $values): array
    {
        $declarations = self::propertiesDeclarations($class_name);
        $convertedValues = [];

        foreach ($values as $property => $value) {
            if (!isset($declarations[$property])) {
                throw new \Exception($class_name . " doesn't define a {$property} property");
            }

            $declaration = $declarations[$property];

            if ($declaration['type'] === 'datetime' && $value !== null) {
                $convertedValues[$property] = $value->format($declaration['format']);
            } elseif ($declaration['type'] === 'boolean') {
                $convertedValues[$property] = (int)$value;
            } elseif ($declaration['type'] === 'json') {
                $convertedValues[$property] = json_encode($value);
            } else {
                $convertedValues[$property] = $value;
            }
        }

        return $convertedValues;
    }

    public static function convertValuesFromDatabase(string $class_name, array $values): array
    {
        $declarations = self::propertiesDeclarations($class_name);
        $convertedValues = [];

        foreach ($values as $property => $value) {
            if (!isset($declarations[$property])) {
                throw new \Exception($class_name . " doesn't define a {$property} property");
            }

            $declaration = $declarations[$property];

            if ($declaration['type'] === 'datetime' && $value !== null) {
                $date = date_create_from_format($declaration['format'], $value);
                $convertedValues[$property] = $date;
            } elseif ($declaration['type'] === 'int') {
                $integer = filter_var($value, FILTER_VALIDATE_INT);
                $convertedValues[$property] = $integer;
            } elseif ($declaration['type'] === 'boolean') {
                $boolean = filter_var($value, FILTER_VALIDATE_BOOLEAN);
                $convertedValues[$property] = $boolean;
            } elseif ($declaration['type'] === 'json') {
                $convertedValues[$property] = json_decode($value, true);
            } else {
                $convertedValues[$property] = $value;
            }
        }

        return $convertedValues;
    }

    public static function propertiesDeclarations(string $class_name): array
    {
        $classReflection = new \ReflectionClass($class_name);
        $properties = $classReflection->getProperties();

        $declarations = [];
        foreach($properties as $property) {
            $column_attributes = $property->getAttributes(Column::class);
            if (empty($column_attributes)) {
                continue;
            }

            $property_name = $property->getName();
            $property_type = $property->getType();
            if ($property_type === null) {
                throw new \Exception($class_name . " must define {$property_name} property type");
            }

            $declaration_type = $property_type->getName();
            if ($declaration_type === 'DateTime') {
                $declaration_type = 'datetime';
            } elseif ($declaration_type === 'array') {
                $declaration_type = 'json';
            }

            $column = $column_attributes[0]->newInstance();
            $declaration = [
                'type' => $declaration_type,
                'computed' => $column->computed,
            ];

            if ($declaration_type === 'datetime' && !isset($declaration['format'])) {
                $declaration['format'] = Column::DATETIME_FORMAT;
            }

            $declarations[$property_name] = $declaration;
        }

        return $declarations;
    }

    public static function dbToModel(string $class_name, array $row): object
    {
        return self::dbToModels($class_name, [$row])[0];
    }

    public static function dbToModels(string $class_name, array $rows): array
    {
        $models = [];

        foreach ($rows as $row) {
            $model = new $class_name();

            $values = self::convertValuesFromDatabase($class_name, $row);
            foreach ($values as $property => $value) {
                $model->$property = $value;
            }

            $models[] = $model;
        }

        return $models;
    }
}
