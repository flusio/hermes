<?php

namespace Minz\Database;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
trait Record
{
    public static function listAll(): array
    {
        return self::listBy([]);
    }

    public static function listBy(array $criteria): array
    {
        $table_name = self::tableName();
        list($sql_where, $params) = Helper::buildWhere($criteria);

        $sql_where_statement = '';
        if ($sql_where) {
            $sql_where_statement = "WHERE {$sql_where}";
        }

        $sql = <<<SQL
            SELECT * FROM {$table_name}
            {$sql_where_statement}
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute($params);
        return Helper::dbToModels(self::class, $statement->fetchAll());
    }

    public static function find(mixed $pk_value): ?object
    {
        $table_name = self::tableName();
        $pk_column = self::primaryKeyColumn();

        $sql = <<<SQL
            SELECT * FROM {$table_name}
            WHERE {$pk_column} = ?
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([$pk_value]);
        $result = $statement->fetch();
        if ($result) {
            return Helper::dbToModel(self::class, $result);
        } else {
            return null;
        }
    }

    public static function findBy(array $criteria): ?object
    {
        $result = self::listBy($criteria);
        if ($result) {
            return $result[0];
        } else {
            return null;
        }
    }

    public static function existsBy(array $criteria): bool
    {
        $table_name = self::tableName();
        list($sql_where, $params) = Helper::buildWhere($criteria);

        $sql_where_statement = '';
        if ($sql_where) {
            $sql_where_statement = "WHERE {$sql_where}";
        }

        $sql = <<<SQL
            SELECT EXISTS (
                SELECT 1 FROM {$table_name}
                {$sql_where_statement}
            )
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute($params);
        return (bool)$statement->fetchColumn();
    }

    public static function exists(mixed $pk_value): bool
    {
        $pk_column = self::primaryKeyColumn();
        return self::existsBy([$pk_column => $pk_value]);
    }

    public static function create(array $values): mixed
    {
        $table_name = self::tableName();
        $values = Helper::convertValuesToDatabase(self::class, $values);

        $properties = array_keys($values);
        $values_as_question_marks = array_fill(0, count($values), '?');
        $values_placeholder = implode(", ", $values_as_question_marks);
        $columns_placeholder = implode(", ", $properties);

        $sql = <<<SQL
            INSERT INTO {$table_name} ({$columns_placeholder})
            VALUES ({$values_placeholder})
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $result = $statement->execute(array_values($values));

        $pk_column = self::primaryKeyColumn();
        if (isset($values[$pk_column])) {
            return $values[$pk_column];
        } else {
            return $database->lastInsertId();
        }
    }

    public static function update(mixed $pk_value, array $values): bool
    {
        $table_name = self::tableName();
        $pk_column = self::primaryKeyColumn();
        $values = Helper::convertValuesToDatabase(self::class, $values);

        $properties = array_keys($values);
        $set_statement_as_array = [];
        foreach ($properties as $property) {
            $set_statement_as_array[] = "{$property} = ?";
        }
        $set_statement = implode(', ', $set_statement_as_array);
        $sql_where = "{$pk_column} = ?";

        $sql = <<<SQL
            UPDATE {$table_name}
            SET {$set_statement}
            WHERE {$sql_where}
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $parameters = array_values($values);
        $parameters[] = $pk_value;
        return $statement->execute($parameters);
    }

    public static function delete(mixed $pk_value): bool
    {
        $table_name = self::tableName();
        $pk_column = self::primaryKeyColumn();
        $sql_where = "{$pk_column} = ?";

        $sql = <<<SQL
            DELETE FROM {$table_name}
            WHERE {$sql_where}
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        return $statement->execute([$pk_value]);
    }

    public function save(): self
    {
        $table_name = self::tableName();
        $pk_column = self::primaryKeyColumn();
        $values = $this->toValues();
        if (isset($values['created_at'])) {
            $pk_value = $this->$pk_column;
            self::update($pk_value, $values);
        } else {
            $values['created_at'] = \Minz\Time::now();

            $pk_value = self::create($values);

            $this->$pk_column = $pk_value;
            $this->created_at = $values['created_at'];
        }

        return $this;
    }

    public function remove(): bool
    {
        $pk_column = self::primaryKeyColumn();
        $pk_value = $this->$pk_column;
        return self::delete($pk_value);
    }

    public static function tableName(): string
    {
        $reflection = new \ReflectionClass(self::class);
        $table_attributes = $reflection->getAttributes(Table::class);
        if (empty($table_attributes)) {
            throw new \Exception(self::class . ' must define a \Minz\Database\Table attribute');
        }
        $table = $table_attributes[0]->newInstance();
        return $table->name;
    }

    public static function primaryKeyColumn(): string
    {
        $reflection = new \ReflectionClass(self::class);
        $table_attributes = $reflection->getAttributes(Table::class);
        if (empty($table_attributes)) {
            throw new \Exception(self::class . ' must define a \Minz\Database\Table attribute');
        }
        $table = $table_attributes[0]->newInstance();
        return $table->primary_key;
    }

    public function toValues(): array
    {
        $declarations = Helper::propertiesDeclarations(self::class);
        $values = [];
        foreach ($declarations as $property => $declaration) {
            if ($declaration['computed']) {
                continue;
            }

            $propertyReflection = new \ReflectionProperty(self::class, $property);
            if ($propertyReflection->isInitialized($this)) {
                $values[$property] = $this->$property;
            }
        }
        return $values;
    }

    public function toDbValues(): array
    {
        return Helper::convertValuesToDatabase(
            self::class,
            $this->toValues(),
        );
    }
}
