<?php

namespace Minz\Database;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
trait Lockable
{
    public function lock(): bool
    {
        $table_name = self::tableName();
        $pk_column = self::primaryKeyColumn();
        $pk_value = $this->$pk_column;

        $sql = <<<SQL
            UPDATE {$table_name}
            SET locked_at = :locked_at
            WHERE {$pk_column} = :pk_value
            AND (locked_at IS NULL OR locked_at <= :lock_timeout)
        SQL;

        $now = \Minz\Time::now();
        $lock_timeout = \Minz\Time::ago(1, 'hour');

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':locked_at' => $now->format(Column::DATETIME_FORMAT),
            ':lock_timeout' => $lock_timeout->format(Column::DATETIME_FORMAT),
            ':pk_value' => $pk_value,
        ]);

        $result = $statement->rowCount() === 1;
        if ($result) {
            $this->locked_at = $now;
        }
        return $result;
    }

    public function unlock(): bool
    {
        $table_name = self::tableName();
        $pk_column = self::primaryKeyColumn();
        $pk_value = $this->$pk_column;

        $sql = <<<SQL
            UPDATE {$table_name}
            SET locked_at = null
            WHERE {$pk_column} = :pk_value
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':pk_value' => $pk_value,
        ]);

        $result = $statement->rowCount() === 1;
        if ($result) {
            $this->locked_at = null;
        }
        return $result;
    }

    public function isLocked(): bool
    {
        if ($this->locked_at === null) {
            return false;
        }

        $lock_timeout = \Minz\Time::ago(1, 'hour');
        return $this->locked_at > $lock_timeout;
    }
}
