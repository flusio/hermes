<?php

namespace Minz\Validable;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
class Unique extends Check
{
    public function assert(): bool
    {
        $property_name = $this->property->getName();
        $property_value = $this->getValue();
        return !$this->instance::existsBy([$property_name => $property_value]);
    }
}
