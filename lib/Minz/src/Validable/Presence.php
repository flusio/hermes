<?php

namespace Minz\Validable;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[\Attribute(\Attribute::TARGET_PROPERTY)]
class Presence extends Check
{
    public function assert(): bool
    {
        $value = $this->getValue();
        return $value !== null && $value !== '';
    }
}
