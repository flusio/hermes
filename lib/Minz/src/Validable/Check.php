<?php

namespace Minz\Validable;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class Check
{
    public string $message = 'Invalid value';

    public \ReflectionProperty $property;

    public object $instance;

    public function __construct(string $message)
    {
        $this->message = $message;
    }

    public function assert(): bool
    {
        throw new \BadMethodCallException("Method assert must be implemented");
    }

    public function getValue(): mixed
    {
        return $this->property->getValue($this->instance);
    }

    public function getMessage(): string
    {
        return $this->message;
    }
}
