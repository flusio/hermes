<?php

namespace Minz\Jobs;

use Minz\Request;
use Minz\Response;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class JobsController
{
    /**
     * List all the current jobs
     *
     * @response 200
     */
    public function index(Request $request): Response
    {
        $jobs = Job::listAll();
        usort($jobs, function ($job_1, $job_2) {
            return $job_1->id - $job_2->id;
        });

        $result = [];
        foreach ($jobs as $job) {
            $job_as_text = "job {$job->id} ({$job->name})";
            $perform_at = $job->perform_at->format(DATE_ATOM);
            if ($job->frequency) {
                $job_as_text .= " scheduled each {$job->frequency}, next at {$perform_at}";
            } else {
                $job_as_text .= " at {$perform_at} {$job->number_attempts} attempts";
            }

            if ($job->locked_at) {
                $job_as_text .= ' (locked)';
            }
            if ($job->failed_at) {
                $job_as_text .= ' (failed)';
            }

            $result[] = $job_as_text;
        }

        return Response::text(200, implode("\n", $result));
    }

    /**
     * Unlock a job.
     *
     * @request_param string id
     *
     * @response 200
     */
    public function unlock(Request $request): Response
    {
        $job_id = $request->param('id');
        $job = Job::find($job_id);
        if (!$job) {
            return Response::text(200, "Job {$job_id} does not exist.");
        }

        if (!$job->isLocked()) {
            return Response::text(200, "Job {$job->id} was not locked.");
        }

        $job->unlock();

        return Response::text(200, "Job {$job->id} lock has been released.");
    }

    /**
     * Find an available job and run it.
     *
     * @request_param string $queue
     *     Selects job in the given queue (default: all). Numbers at the end of
     *     the queue are ignored, so it allows to identify worker with, e.g.
     *     fetchers1, fetchers2, etc.
     *
     * @response 204 If no job to run
     * @response 500 If we took a job but we can't lock it
     * @response 200
     */
    public function run(Request $request): Response
    {
        $queue = $request->param('queue', 'all');
        $queue = rtrim($queue, '0..9');
        $job = Job::findNextJob($queue);
        if (!$job) {
            return Response::noContent();
        }

        if (!$job->lock()) {
            return Response::internalServerError();
        }

        $time_start = microtime(true);

        $job->number_attempts += 1;
        $job->save();

        try {
            $handler_class = $job->handler['job_class'];
            $handler_args = $job->handler['job_args'];
            $handler = new $handler_class();
            $handler->perform(...$handler_args);

            if ($job->frequency) {
                $job->reschedule();
                $job->unlock();
            } else {
                $job->remove();
            }
            $error = false;
        } catch (\Exception $exception) {
            $job->fail((string)$exception);
            $job->unlock();
            $error = true;
        }

        $time_end = microtime(true);
        $time = number_format($time_end - $time_start, 3);

        if ($error) {
            return Response::text(500, "job#{$job->id} ({$job->name}): failed (in {$time} seconds)");
        } else {
            return Response::text(200, "job#{$job->id} ({$job->name}): done (in {$time} seconds)");
        }
    }

    /**
     * Start a job worker which call `run()` in a loop. This action should be
     * called via a systemd service, or as any other kind of "init" service.
     *
     * Responses are yield during the lifetime of the action.
     *
     * @request_param string $queue
     *     Selects job in the given queue (default: all). Numbers at the end of
     *     the queue are ignored, so it allows to identify worker with, e.g.
     *     fetchers1, fetchers2, etc.
     *
     * @response 204 If no job to run
     * @response 500 If we took a job but we can't lock it
     * @response 200
     */
    public function watch(Request $request): \Generator
    {
        \pcntl_async_signals(true);
        \pcntl_signal(SIGTERM, [$this, 'stopWatch']);
        \pcntl_signal(SIGINT, [$this, 'stopWatch']);
        $this->exit_watch = false;

        $queue = $request->param('queue', 'all');
        yield Response::text(200, "[Job worker ({$queue}) started]");

        while (true) {
            yield $this->run($request);

            $database = \Minz\Database::get();
            $database->close();

            if (!$this->exit_watch) {
                sleep(5);
            }

            // exit_watch can be set to true during sleep(), so don't merge the
            // two conditions with a "else"!
            if ($this->exit_watch) {
                break;
            }
        }

        yield Response::text(200, "[Job worker ({$queue}) stopped]");
    }

    /**
     * Handler to catch signals and stop the worker.
     */
    public function stopWatch(): void
    {
        $this->exit_watch = true;
    }
}
