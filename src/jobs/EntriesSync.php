<?php

namespace App\jobs;

use App\models;
use App\services;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class EntriesSync extends \Minz\Jobs\Job
{
    use Installable;

    public function __construct()
    {
        parent::__construct();
        $this->frequency = '+15 seconds';
        $this->queue = 'fetchers';
    }

    public function perform(): void
    {
        $entry_fetcher_service = new services\EntryFetcher();

        $entries = models\Entry::listToFetch(25);
        foreach ($entries as $entry) {
            $has_lock = $entry->lock();
            if (!$has_lock) {
                continue;
            }

            try {
                $entry_fetcher_service->fetch($entry);
            } catch (\Exception $e) {
                \Minz\Log::error("Error while syncing entry {$entry->id}: {$e->getMessage()}");
            }

            $entry->unlock();
        }
    }
}
