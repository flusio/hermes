<?php

namespace App\jobs;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
trait Installable
{
    public static function install()
    {
        $job = self::findBy(['name' => self::class]);
        if (!$job) {
            $job = new self();
            $job->performLater();
        }
    }
}
