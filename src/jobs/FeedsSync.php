<?php

namespace App\jobs;

use App\models;
use App\services;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class FeedsSync extends \Minz\Jobs\Job
{
    use Installable;

    public function __construct()
    {
        parent::__construct();
        $this->frequency = '+1 hour';
        $this->queue = 'fetchers';
    }

    public function perform()
    {
        $feed_fetcher_service = new services\FeedFetcher();

        $feeds = models\Feed::listAll();
        foreach ($feeds as $feed) {
            $has_lock = $feed->lock();
            if (!$has_lock) {
                continue;
            }

            try {
                $feed_fetcher_service->fetch($feed);
            } catch (\Exception $e) {
                \Minz\Log::error("Error while syncing feed {$feed->id}: {$e->getMessage()}");
            }

            $feed->unlock();
        }
    }
}
