<?php

namespace App\jobs;

use App\models;
use App\services;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class Cleaner extends \Minz\Jobs\Job
{
    use Installable;

    public function __construct()
    {
        parent::__construct();
        $this->perform_at = \Minz\Time::relative('tomorrow 1:00');
        $this->frequency = '+1 day';
    }

    public function perform()
    {
        $cache = new \SpiderBits\Cache(\Minz\Configuration::$application['cache_path']);
        $cache->clean();

        models\Entry::cleanOlderThan(\Minz\Time::ago(1, 'week'));

        $image_service = new services\Image();
        $image_service->clean();
    }
}
