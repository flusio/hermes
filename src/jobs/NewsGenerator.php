<?php

namespace App\jobs;

use App\models;
use App\services;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class NewsGenerator extends \Minz\Jobs\Job
{
    use Installable;

    public function __construct()
    {
        parent::__construct();
        $this->perform_at = \Minz\Time::relative('today 7:00');
        $this->frequency = '+1 day';
    }

    public function perform()
    {
        $news_service = new services\NewsGenerator();
        $news_service->generate();
    }
}
