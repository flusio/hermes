<?php

namespace App\auth;

use App\models\Person;
use App\models\Token;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class User
{
    private static ?Person $instance = null;

    public static function get(): ?Person
    {
        if (!self::sessionTokenExists()) {
            return null;
        }

        if (self::$instance !== null) {
            return self::$instance;
        }

        $person = Person::findBy([
            'session_token_id' => $_SESSION['session_token_id'],
        ]);

        if (!$person) {
            unset($_SESSION['session_token_id']);
            return null;
        }

        $session_token = $person->sessionToken();
        if ($session_token->hasExpired()) {
            unset($_SESSION['session_token_id']);
            return null;
        }

        self::$instance = $person;
        return self::$instance;
    }

    public static function setSessionToken(Token $token): void
    {
        self::reset();

        if ($token->hasExpired()) {
            \Minz\Log::warning('Trying to setSessionToken an expired token.');
            return;
        }

        $_SESSION['session_token_id'] = $token->id;
    }

    public static function setSessionTokenId(string $token_id): void
    {
        $token = Token::find($token_id);
        if ($token) {
            self::setSessionToken($token);
        }
    }

    public static function sessionTokenExists(): bool
    {
        return isset($_SESSION['session_token_id']);
    }

    public static function reset()
    {
        unset($_SESSION['session_token_id']);
        self::$instance = null;
    }
}
