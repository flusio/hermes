<?= '<?xml version="1.0" encoding="UTF-8" ?>' . "\n" ?>
<feed xmlns="http://www.w3.org/2005/Atom">
    <title><?= $brand ?></title>

    <link href="<?= url_full('home') ?>" rel="alternate" type="text/html" />
    <link href="<?= url_full('web feed') ?>" rel="self" type="application/atom+xml" />

    <id><?= sha1(url_full('home')) ?></id>
    <generator><?= $user_agent ?></generator>

    <?php if (isset($entries[0])): ?>
        <updated><?= $entries[0]->published_at->format(\DateTimeInterface::ATOM) ?></updated>
    <?php else: ?>
        <updated><?= \Minz\Time::now()->format(\DateTimeInterface::ATOM) ?></updated>
    <?php endif; ?>

    <?php foreach ($entries as $entry): ?>
        <entry>
            <title><?= protect($entry->title) ?></title>
            <id><?= $entry->tagUri() ?></id>

            <link href="<?= protect($entry->url) ?>" rel="alternate" type="text/html" />

            <published><?= $entry->published_at->format(\DateTimeInterface::ATOM) ?></published>
            <updated><?= $entry->published_at->format(\DateTimeInterface::ATOM) ?></updated>

            <content type="html"><![CDATA[
                <a href="<?= protect($entry->url) ?>">
                    Lire sur <?= protect($entry->feed()->name) ?>
                </a>
            ]]></content>
        </entry>
    <?php endforeach; ?>
</feed>
