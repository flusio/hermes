<?php

namespace App\services;

use App\models;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class NewsGenerator
{
    public function generate(): void
    {
        $yesterday = \Minz\Time::relative('yesterday 7:00');
        $today = \Minz\Time::relative('today 7:00');
        $entries = models\Entry::listToMarkForNews($yesterday, $today);

        usort($entries, function ($entry1, $entry2) {
            return $entry2->votes_count - $entry1->votes_count;
        });

        $selected_entries = [];
        foreach ($entries as $entry) {
            if (count($selected_entries) >= 10) {
                break;
            }

            $selected_entries[] = $entry;
        }

        models\Entry::markForNews($selected_entries, $today);
    }
}
