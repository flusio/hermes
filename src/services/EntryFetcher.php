<?php

namespace App\services;

use App\models;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class EntryFetcher
{
    /** @var \SpiderBits\Cache */
    private $cache;

    /** @var \SpiderBits\Http */
    private $http;

    /** @var array */
    private $options = [
        'timeout' => 10,
        'cache' => true,
        'force_sync' => false,
    ];

    /**
     * @param array $options
     *     A list of options where possible keys are:
     *     - timeout (integer)
     *     - cache (boolean)
     *     - force_sync (boolean)
     */
    public function __construct(array $options = [])
    {
        $this->options = array_merge($this->options, $options);

        $cache_path = \Minz\Configuration::$application['cache_path'];
        $this->cache = new \SpiderBits\Cache($cache_path);

        $this->http = new \SpiderBits\Http();
        $this->http->user_agent = \Minz\Configuration::$application['user_agent'];
        $this->http->timeout = $this->options['timeout'];
    }

    public function fetch(models\Entry $entry): void
    {
        $info = $this->fetchUrl($entry->url);

        $entry->fetched_at = \Minz\Time::now();
        $entry->fetched_code = $info['status'];
        $entry->fetched_error = '';
        $entry->fetched_count += 1;
        if (isset($info['error'])) {
            $entry->fetched_error = $info['error'];
            $entry->save();
            return;
        }

        // we set the title only if it wasn't changed yet
        $title_never_changed = $entry->title === $entry->url;
        if (isset($info['title']) && $title_never_changed) {
            $entry->title = $info['title'];
        }

        $reading_never_changed = $entry->reading_time <= 0;
        if (isset($info['reading_time']) && $reading_never_changed) {
            $entry->reading_time = $info['reading_time'];
        }

        if (!empty($info['links'])) {
            $entry->links = $info['links'];
        }

        $illustration_never_set = empty($link->image_filename);
        if (isset($info['url_illustration']) && $illustration_never_set) {
            $image_service = new Image();
            $image_filename = $image_service->generatePreviews($info['url_illustration']);
            $entry->image_filename = $image_filename;
        }

        $entry->save();
    }

    /**
     * Fetch URL content and return information about the page
     *
     * @param string $url
     *
     * @return array Possible keys are:
     *     - status (always)
     *     - error
     *     - title
     *     - reading_time
     *     - url_illustration
     *     - url_feeds
     */
    public function fetchUrl(string $url): array
    {
        // We "GET" the URL...
        $url_hash = \SpiderBits\Cache::hash($url);
        $cached_response = $this->cache->get($url_hash);
        if ($this->options['cache'] && $cached_response) {
            // ... via the cache
            $response = \SpiderBits\Response::fromText($cached_response);
        } else {
            // ... or via HTTP
            try {
                $response = $this->http->get($url, [], [
                    'max_size' => 20 * 1024 * 1024,
                ]);
            } catch (\SpiderBits\HttpError $e) {
                return [
                    'status' => 0,
                    'error' => $e->getMessage(),
                ];
            }

            // that we add to cache on success
            if ($response->success) {
                $this->cache->save($url_hash, (string)$response);
            }
        }

        $info = [
            'status' => $response->status,
            'links' => [],
        ];

        // It's dangerous out there. mb_convert_encoding makes sure data is a
        // valid UTF-8 string.
        $encodings = mb_list_encodings();
        $data = mb_convert_encoding($response->data, 'UTF-8', $encodings);

        if (!$response->success) {
            // Okay, Houston, we've had a problem here. Return early, there's
            // nothing more to do.
            $info['error'] = $data;
            return $info;
        }

        $content_type = $response->header('content-type');
        if ($content_type && !str_contains($content_type, 'text/html')) {
            // We operate on HTML only. If content type is not declared, we
            // examine data hoping for HTML.
            return $info;
        }

        $dom = \SpiderBits\Dom::fromText($data);

        // Parse the title from the DOM
        $title = \SpiderBits\DomExtractor::title($dom);
        if ($title) {
            $info['title'] = $title;
        }

        // And roughly estimate the reading time
        $content = \SpiderBits\DomExtractor::content($dom);
        $words = array_filter(explode(' ', $content));
        $info['reading_time'] = intval(count($words) / 200);

        // Get the illustration URL if any
        $url_illustration = \SpiderBits\DomExtractor::illustration($dom);
        $url_illustration = \SpiderBits\Url::sanitize($url_illustration);
        if ($url_illustration) {
            $info['url_illustration'] = $url_illustration;
        }

        $url_feeds = \SpiderBits\DomExtractor::feeds($dom);
        foreach ($url_feeds as $url_feed) {
            $url_feed = \SpiderBits\Url::absolutize($url_feed, $url);
            $url_feed = \SpiderBits\Url::sanitize($url_feed);
            $info['links']['feeds'][] = $url_feed;
        }

        return $info;
    }
}
