<?php

namespace App\services;

use App\models;
use App\utils;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class Image
{
    private \SpiderBits\Http $http;

    private string $path_images;

    public function __construct()
    {
        $this->http = new \SpiderBits\Http();
        $this->http->user_agent = \Minz\Configuration::$application['user_agent'];
        $this->http->timeout = 10;

        $this->path_images = \Minz\Configuration::$application['media_path'];
    }

    public function generatePreviews(string $image_url): string
    {
        $url_hash = \SpiderBits\Cache::hash($image_url);
        $subpath = utils\Image::filenameToSubpath($url_hash);
        $image_path = "{$this->path_images}/{$subpath}";
        $image_filepath = "{$image_path}/{$url_hash}";

        if (!file_exists($image_path)) {
            @mkdir($image_path, 0755, true);
        }

        $file_exists = glob($image_filepath . '.*');
        if ($file_exists) {
            return basename($file_exists[0]);
        }

        try {
            $options = [
                'max_size' => 20 * 1024 * 1024,
            ];
            $response = $this->http->get($image_url, [], $options);
        } catch (\SpiderBits\HttpError $e) {
            return '';
        }

        if (!$response->success) {
            return '';
        }

        try {
            $image = utils\Image::fromString($response->data);
            $image->resize(720, 405);
            $image->save($image_filepath . '.' . $image->type());

            return $url_hash . '.' . $image->type();
        } catch (\DomainException $e) {
            \Minz\Log::warning("Can’t save preview image ({$image_url})");
            return '';
        }
    }

    public function clean(): void
    {
        $subdir_names_depth_1 = scandir($this->path_images, SCANDIR_SORT_NONE);
        $subdir_names_depth_1 = array_filter($subdir_names_depth_1, function ($subdir_name) {
            // Exclude files or dirs starting with a dot.
            return $subdir_name[0] !== '.';
        });

        // Iterate over the first level of media subdirectories
        foreach ($subdir_names_depth_1 as $index => $subdir_name) {
            // get the full list of file names under the current subdirectory
            $subdir_path = "{$this->path_images}/{$subdir_name}";
            $file_paths_on_fs = glob("{$subdir_path}/???/???/*", GLOB_NOSORT);
            $file_names_on_fs = array_map('basename', $file_paths_on_fs);

            // get the list of file names starting with the same 3 characters
            // in database
            $file_names_from_db = models\Entry::listImageFilenamesStartingWith($subdir_name);

            // do the diff between those 2 arrays to get the list of files to
            // delete (i.e. if a filename exists on the filesystem, but not in
            // database, the file can be deleted)
            $file_names_to_delete = array_diff($file_names_on_fs, $file_names_from_db);

            // finally, delete the files
            foreach ($file_names_to_delete as $file_name) {
                $sub_path = utils\Image::filenameToSubpath($file_name);
                @unlink("{$this->path_images}/{$sub_path}/{$file_name}");
            }
        }
    }
}
