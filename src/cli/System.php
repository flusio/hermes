<?php

namespace App\cli;

use Minz\Response;

class System
{
    /**
     * Initialize the database and set the migration version.
     *
     * @param \Minz\Request $request
     *
     * @return \Minz\Response
     */
    public function init($request)
    {
        $app_path = \Minz\Configuration::$app_path;
        $migrations_path = $app_path . '/src/migrations';
        $migrations_version_path = $app_path . '/data/migrations_version.txt';

        if (file_exists($migrations_version_path)) {
            return $this->update($request);
        } else {
            $schema = file_get_contents($app_path . '/src/schema.sql');
            $database = \Minz\Database::get();
            $database->exec($schema);

            $migrator = new \Minz\Migrator($migrations_path);
            $version = $migrator->lastVersion();
            $saved = @file_put_contents($migrations_version_path, $version);
            if ($saved === false) {
                return Response::text(
                    500,
                    "Cannot save data/migrations_version.txt file ({$version}).",
                );
            }

            return Response::text(200, 'The system has been initialized.');
        }
    }

    /**
     * Execute the migrations under src/migrations/. The version is stored in
     * data/migrations_version.txt.
     *
     * @param \Minz\Request $request
     *
     * @return \Minz\Response
     */
    public function update($request)
    {
        $app_path = \Minz\Configuration::$app_path;
        $migrations_path = $app_path . '/src/migrations';
        $migrations_version_path = $app_path . '/data/migrations_version.txt';

        if (!file_exists($migrations_version_path)) {
            return $this->init($request);
        }

        $migration_version = @file_get_contents($migrations_version_path);
        if ($migration_version === false) {
            return Response::text(
                500,
                'Cannot read data/migrations_version.txt file.',
            );
        }

        $migrator = new \Minz\Migrator($migrations_path);
        if ($migration_version) {
            $migrator->setVersion($migration_version);
        }

        if ($migrator->upToDate()) {
            return Response::text(200, 'The system is already up to date.');
        }

        $results = $migrator->migrate();

        $new_version = $migrator->version();
        $saved = @file_put_contents($migrations_version_path, $new_version);
        if ($saved === false) {
            return Response::text(
                500,
                "Cannot save data/migrations_version.txt file ({$new_version}).",
            );
        }

        $has_error = false;
        $output = '';
        foreach ($results as $migration => $result) {
            if ($result === true) {
                $output = "{$migration}: OK";
            } elseif ($result === false) {
                $output = "{$migration}: KO";
                $has_error = true;
            } else {
                $output = "{$migration}: {$result}";
                $has_error = true;
            }
        }

        return Response::text($has_error ? 500 : 200, $output);
    }
}
