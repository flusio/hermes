<?php

namespace App\cli;

use App\services;
use Minz\Request;
use Minz\Response;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class News
{
    public function generate(Request $request): Response
    {
        $news_service = new services\NewsGenerator();
        $news_service->generate();

        return \Minz\Response::text(200, 'News generated.');
    }
}
