<?php

namespace App\cli;

use Minz\Request;
use Minz\Response;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class Jobs extends \Minz\Jobs\JobsController
{
    /**
     * (Re-)install the jobs.
     *
     * @response 200
     */
    public function install(Request $request): Response
    {
        \App\jobs\Cleaner::install();
        \App\jobs\NewsGenerator::install();
        \App\jobs\EntriesSync::install();
        \App\jobs\FeedsSync::install();
        return Response::text(200, 'Jobs installed.');
    }
}
