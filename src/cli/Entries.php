<?php

namespace App\cli;

use App\models;
use App\services;
use Minz\Request;
use Minz\Response;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class Entries
{
    public function index(Request $request): Response
    {
        $entries = models\Entry::listAll();
        $entries_as_text = [];
        foreach ($entries as $entry) {
            $entry_as_text = "{$entry->id} {$entry->url}";
            if (!$entry->isFetched()) {
                $entry_as_text .= ' (to sync)';
            }

            if ($entry->isLocked()) {
                $entry_as_text .= ' (locked)';
            }

            $entries_as_text[] = $entry_as_text;
        }

        if (!$entries_as_text) {
            $entries_as_text[] = 'No entries to list.';
        }

        return Response::text(200, implode("\n", $entries_as_text));
    }

    public function sync(Request $request): Response
    {
        $id = $request->param('id');
        $nocache = $request->paramBoolean('nocache', false);
        $entry = models\Entry::find($id);
        if (!$entry) {
            return Response::text(404, "Entry id `{$id}` does not exist.");
        }

        $entry_fetcher_service = new services\EntryFetcher([
            'cache' => !$nocache,
        ]);
        $entry_fetcher_service->fetch($entry);

        return Response::text(200, "Feed {$id} ({$entry->url}) has been synchronized.");
    }

    public function unlock(Request $request): Response
    {
        $id = $request->param('id');
        $entry = models\Entry::find($id);
        if (!$entry) {
            return Response::text(404, "Entry id `{$id}` does not exist.");
        }

        if (!$entry->isLocked()) {
            return Response::text(200, "Entry {$entry->id} ({$entry->url}) was not locked.");
        }

        $entry->unlock();

        return Response::text(200, "Entry {$entry->id} ({$entry->url}) has been unlocked.");
    }
}
