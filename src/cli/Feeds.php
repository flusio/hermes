<?php

namespace App\cli;

use App\models;
use App\services;
use Minz\Request;
use Minz\Response;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class Feeds
{
    public function index(Request $request): Response
    {
        $feeds = models\Feed::listAll();
        $feeds_as_text = [];
        foreach ($feeds as $feed) {
            $feed_as_text = "{$feed->id} {$feed->url}";
            if ($feed->isLocked()) {
                $feed_as_text .= ' (locked)';
            }

            $feeds_as_text[] = $feed_as_text;
        }

        if (!$feeds_as_text) {
            $feeds_as_text[] = 'No feeds to list.';
        }

        return Response::text(200, implode("\n", $feeds_as_text));
    }

    public function add(Request $request): Response
    {
        $url = $request->param('url');
        $url = \SpiderBits\Url::sanitize($url);

        $existing_feed = models\Feed::findBy(['url' => $url]);
        if ($existing_feed) {
            return Response::text(400, 'Feed already exists in database.');
        }

        // create the feed before fetching it
        $feed = models\Feed::init($url);
        $feed->save();

        $fetch_service = new services\FeedFetcher();
        $fetch_service->fetch($feed);

        if ($feed->fetched_error) {
            $error = $feed->fetched_error;
            return Response::text(400, "Feed {$feed->url} has been added but cannot be fetched: {$error}.");
        }

        return Response::text(200, "Feed {$feed->url} ({$feed->name}) has been added.");
    }

    public function sync(Request $request): Response
    {
        $id = $request->param('id');
        $nocache = $request->paramBoolean('nocache', false);
        $feed = models\Feed::find($id);
        if (!$feed) {
            return Response::text(404, "Feed id `{$id}` does not exist.");
        }

        $feed_fetcher_service = new services\FeedFetcher([
            'cache' => !$nocache,
        ]);
        $feed_fetcher_service->fetch($feed);

        return Response::text(200, "Feed {$id} ({$feed->url}) has been synchronized.");
    }

    public function unlock(Request $request): Response
    {
        $id = $request->param('id');
        $feed = models\Feed::find($id);
        if (!$feed) {
            return Response::text(404, "Feed id `{$id}` does not exist.");
        }

        if (!$feed->isLocked()) {
            return Response::text(200, "Feed {$feed->id} ({$feed->url}) was not locked.");
        }

        $feed->unlock();

        return Response::text(200, "Feed {$feed->id} ({$feed->url}) has been unlocked.");
    }
}
