<?php

function url_media(string $filename, string $default = 'default-card.png'): string
{
    if (!$filename) {
        return url_static($default);
    }

    $media_path = \Minz\Configuration::$application['media_path'];
    $subpath = \App\utils\Image::filenameToSubpath($filename);
    $filepath = "{$media_path}/{$subpath}/{$filename}";
    $modification_time = @filemtime($filepath);
    $file_url = \Minz\Url::path() . "/static/media/{$subpath}/{$filename}";
    if ($modification_time) {
        return $file_url . '?' . $modification_time;
    } else {
        return url_static($default);
    }
}
