<?php

namespace App\models;

use Minz\Database;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[Database\Table(name: 'entries')]
class Entry
{
    use Database\Record;
    use Database\Lockable;
    use BulkQueries;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTime $created_at;

    #[Database\Column]
    public string $url;

    #[Database\Column]
    public string $url_hash;

    #[Database\Column]
    public string $title;

    #[Database\Column]
    public \DateTime $published_at;

    #[Database\Column]
    public int $reading_time = 0;

    #[Database\Column]
    public array $links = [];

    #[Database\Column]
    public string $image_filename = '';

    #[Database\Column]
    public int $feed_id;

    #[Database\Column]
    public string $feed_entry_id = '';

    #[Database\Column]
    public ?\DateTime $in_news_of = null;

    #[Database\Column]
    public ?\DateTime $locked_at = null;

    #[Database\Column]
    public ?\DateTime $fetched_at = null;

    #[Database\Column]
    public int $fetched_code = 0;

    #[Database\Column]
    public string $fetched_error = '';

    #[Database\Column]
    public int $fetched_count = 0;

    #[Database\Column(computed: true)]
    public int $votes_count = 0;

    public ?Feed $feed = null;

    public static function init(string $url, Feed $feed): Entry
    {
        $entry = new self();
        $entry->url = $url;
        $entry->url_hash = sha1($url);
        $entry->title = $url;
        $entry->published_at = \Minz\Time::now();
        $entry->feed_id = $feed->id;
        return $entry;
    }

    public function tagUri(): string
    {
        $host = \Minz\Configuration::$url_options['host'];
        $date = $this->created_at->format('Y-m-d');
        return "tag:{$host},{$date}:entry/{$this->id}";
    }

    public function feed(): Feed
    {
        if ($this->feed === null) {
            $this->feed = Feed::find($this->feed_id);
        }
        return $this->feed;
    }

    public static function getUrlsMapping(Feed $feed): array
    {
        $sql = <<<SQL
            SELECT e.url, e.id FROM entries e
            WHERE e.feed_id = :feed_id
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':feed_id' => $feed->id,
        ]);

        return $statement->fetchAll(\PDO::FETCH_KEY_PAIR);
    }

    public static function getFeedEntryIdsMapping(Feed $feed): array
    {
        $sql = <<<SQL
            SELECT e.feed_entry_id, e.id, e.url
            FROM entries e
            WHERE e.feed_id = :feed_id
            ORDER BY e.published_at DESC, e.id
            LIMIT 1000
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':feed_id' => $feed->id,
        ]);

        return $statement->fetchAll(\PDO::FETCH_UNIQUE);
    }

    public static function listToFetch(int $max_number)
    {
        $table_name = self::tableName();
        $sql = <<<SQL
            SELECT * FROM {$table_name}
            WHERE fetched_at IS NULL
            OR (
                (fetched_code < 200 OR fetched_code >= 300)
                AND fetched_count <= 25
                AND fetched_at < date('now', '-' || fetched_count || ' minutes')
            )
            ORDER BY random()
            LIMIT ?
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            $max_number,
        ]);
        return Database\Helper::dbToModels(self::class, $statement->fetchAll());
    }

    public function isFetched(): bool
    {
        return !($this->fetched_at === null || (
            ($this->fetched_code < 200 && $this->fetched_code >= 300) &&
            $this->fetched_count <= 25 &&
            $this->fetched_at < \Minz\Time::ago($this->fetched_count, 'minutes')
        ));
    }

    public static function listForFeed(Person $person, \DateTime $after_date): array
    {
        $table_name = self::tableName();
        $sql = <<<SQL
            SELECT e.* FROM {$table_name} e
            WHERE e.published_at >= :after_date
            AND id NOT IN (
                SELECT v.entry_id FROM votes v
                WHERE v.person_id = :person_id
            )
            ORDER BY e.published_at ASC, e.id
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':after_date' => $after_date->format(Database\Column::DATETIME_FORMAT),
            ':person_id' => $person->id,
        ]);
        return Database\Helper::dbToModels(self::class, $statement->fetchAll());
    }

    public static function listForNews(\DateTime $after_date): array
    {
        $table_name = self::tableName();
        $sql = <<<SQL
            SELECT e.*, SUM(v.strength) AS votes_count
            FROM entries e
            LEFT JOIN votes v ON e.id = v.entry_id
            WHERE e.in_news_of IS NOT NULL
            AND e.in_news_of >= :after_date
            GROUP BY e.id
            ORDER BY votes_count DESC, e.published_at, e.id
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':after_date' => $after_date->format(Database\Column::DATETIME_FORMAT),
        ]);
        return Database\Helper::dbToModels(self::class, $statement->fetchAll());
    }

    public static function listToMarkForNews(\DateTime $after_date, \DateTime $before_date): array
    {
        $table_name = self::tableName();
        $sql = <<<SQL
            SELECT e.*, SUM(v.strength) AS votes_count
            FROM entries e, votes v
            WHERE e.id = v.entry_id
            AND e.published_at >= :after_date
            AND e.published_at < :before_date
            GROUP BY e.id
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':after_date' => $after_date->format(Database\Column::DATETIME_FORMAT),
            ':before_date' => $before_date->format(Database\Column::DATETIME_FORMAT),
        ]);
        return Database\Helper::dbToModels(self::class, $statement->fetchAll());
    }

    public static function markForNews(array $entries, \DateTime $date): bool
    {
        $params = array_merge(
            [$date->format(Database\Column::DATETIME_FORMAT)],
            array_column($entries, 'id'),
        );

        $question_marks = array_fill(0, count($entries), '?');
        $in_statement = implode(',', $question_marks);

        $sql = <<<SQL
            UPDATE entries
            SET in_news_of = ?
            WHERE id IN ({$in_statement})
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        return $statement->execute($params);
    }

    public static function cleanOlderThan(\DateTime $date): bool
    {
        $sql = <<<SQL
            DELETE FROM entries
            WHERE published_at < ?
            AND in_news_of IS NULL
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        return $statement->execute([
            $date->format(Database\Column::DATETIME_FORMAT),
        ]);
    }

    public static function listImageFilenamesStartingWith(string $prefix): array
    {
        $sql = <<<SQL
            SELECT image_filename FROM entries
            WHERE image_filename LIKE ?
        SQL;

        $database = \Minz\Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            $prefix . '%',
        ]);
        return $statement->fetchAll(\PDO::FETCH_COLUMN);
    }

    public static function validateUrl(string $url): bool
    {
        $validate = filter_var($url, FILTER_VALIDATE_URL) !== false;
        if (!$validate) {
            return false;
        }

        $parsed_url = parse_url($url);
        if ($parsed_url['scheme'] !== 'http' && $parsed_url['scheme'] !== 'https') {
            return false;
        }

        if (isset($parsed_url['pass'])) {
            return false;
        }

        return true;
    }
}
