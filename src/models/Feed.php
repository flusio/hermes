<?php

namespace App\models;

use Minz\Database;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[Database\Table(name: 'feeds')]
class Feed
{
    use Database\Record;
    use Database\Lockable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTime $created_at;

    #[Database\Column]
    public string $url;

    #[Database\Column]
    public string $type = 'unknown';

    #[Database\Column]
    public string $name;

    #[Database\Column]
    public string $description = '';

    #[Database\Column]
    public array $links = [];

    #[Database\Column]
    public string $image_filename = '';

    #[Database\Column]
    public ?\DateTime $image_fetched_at = null;

    #[Database\Column]
    public string $last_hash = '';

    #[Database\Column]
    public ?\DateTime $locked_at = null;

    #[Database\Column]
    public ?\DateTime $fetched_at = null;

    #[Database\Column]
    public int $fetched_code = 0;

    #[Database\Column]
    public string $fetched_error = '';

    public static function init(string $url): Feed
    {
        $feed = new self();
        $feed->url = $url;
        $feed->name = $url;
        return $feed;
    }
}
