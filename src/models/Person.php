<?php

namespace App\models;

use App\utils;
use Minz\Database;
use Minz\Validable;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[Database\Table(name: 'people')]
class Person
{
    use Database\Record;
    use Validable;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTime $created_at;

    #[Database\Column]
    #[Validable\Presence(message: 'Saisissez un pseudonyme.')]
    #[Validable\Length(max: 42, message: 'Saississez un pseudonyme de moins de {max} caractères.')]
    #[Validable\Unique(message: 'Ce pseudonyme est déjà utilisé, choisissez-en un différent.')]
    public string $nickname;

    #[Database\Column]
    #[Validable\Presence(message: 'Saisissez un mot de passe.')]
    public string $password_hash;

    #[Database\Column]
    public ?string $session_token_id = null;

    public ?Token $session_token = null;

    public static function init(string $nickname, string $password): Person
    {
        $person = new self();
        $person->nickname = $nickname;
        $person->password_hash = self::hashPassword($password);
        return $person;
    }

    public static function hashPassword(string $password): string
    {
        if (empty($password)) {
            return '';
        }

        return password_hash($password, PASSWORD_BCRYPT);
    }

    public function verifyPassword(string $password): bool
    {
        return password_verify($password, $this->password_hash);
    }

    public function sessionToken(): ?Token
    {
        if (!$this->session_token_id) {
            return null;
        }

        if ($this->session_token === null) {
            $this->session_token = Token::find($this->session_token_id);
        }

        return $this->session_token;
    }

    public function renewSession(): Token
    {
        $session_token = $this->sessionToken();
        if ($session_token) {
            $session_token->remove();
        }

        $token = Token::init(1, 'month');
        $this->session_token = $token;
        $this->session_token_id = $token->id;

        return $token;
    }

    public function resetSession(): void
    {
        $session_token = $this->sessionToken();
        if (!$session_token) {
            return;
        }

        $session_token->remove();
        $this->session_token = null;
        $this->session_token_id = null;
    }

    public function voteFor(Entry $entry): ?Vote
    {
        return Vote::findBy([
            'person_id' => $this->id,
            'entry_id' => $entry->id,
        ]);
    }

    public function hasVotedOn(Entry $entry): bool
    {
        return Vote::existsBy([
            'person_id' => $this->id,
            'entry_id' => $entry->id,
        ]);
    }

    public function listFeedEntriesForToday(): array
    {
        $date = utils\Date::getBeginningDay();
        return Entry::listForFeed($this, $date);
    }

    public function remainingPositiveVotesForToday(): int
    {
        $date = utils\Date::getBeginningDay();
        $votes_count = Vote::countForPersonAfter($this, $date);
        return max(20 - $votes_count, 0);
    }
}
