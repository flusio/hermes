<?php

namespace App\models;

use Minz\Database;

#[Database\Table(name: 'tokens')]
class Token
{
    use Database\Record;

    #[Database\Column]
    public string $id;

    #[Database\Column]
    public \DateTime $created_at;

    #[Database\Column]
    public \DateTime $expired_at;

    public static function init(int $number, string $duration, int $length = 64): Token
    {
        $token = new self();
        $token->id = \Minz\Random::hex($length);
        $token->expired_at = \Minz\Time::fromNow($number, $duration);
        return $token;
    }

    public function hasExpired(): bool
    {
        return $this->expiresIn(0, 'seconds');
    }

    public function expiresIn(int $number, string $unit): bool
    {
        return \Minz\Time::fromNow($number, $unit) >= $this->expired_at;
    }
}
