<?php

namespace App\models;

use Minz\Database;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
#[Database\Table(name: 'votes')]
class Vote
{
    use Database\Record;

    #[Database\Column]
    public int $id;

    #[Database\Column]
    public \DateTime $created_at;

    #[Database\Column]
    public int $person_id;

    #[Database\Column]
    public int $entry_id;

    #[Database\Column]
    public int $strength;

    public static function init(Person $person, Entry $entry, int $strength): Vote
    {
        $vote = new self();
        $vote->person_id = $person->id;
        $vote->entry_id = $entry->id;
        $vote->strength = $strength;
        return $vote;
    }

    public static function countForPersonAfter(Person $person, \DateTime $after_date)
    {
        $sql = <<<SQL
            SELECT COUNT(*) FROM votes
            WHERE person_id = :person_id
            AND created_at >= :after_date
            AND strength > 0
        SQL;

        $database = Database::get();
        $statement = $database->prepare($sql);
        $statement->execute([
            ':person_id' => $person->id,
            ':after_date' => $after_date->format(Database\Column::DATETIME_FORMAT),
        ]);

        return intval($statement->fetchColumn());
    }
}
