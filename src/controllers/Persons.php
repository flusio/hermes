<?php

namespace App\controllers;

use App\auth;
use App\models;
use \Minz\Request;
use \Minz\Response;

class Persons
{
    public function new(Request $request): Response
    {
        if (auth\User::get()) {
            return \Minz\Response::redirect('home');
        }

        return \Minz\Response::ok('persons/new.phtml', [
            'nickname' => '',
            'password' => '',
        ]);
    }

    public function create(Request $request): Response
    {
        if (auth\User::get()) {
            return \Minz\Response::redirect('home');
        }

        $csrf = $request->param('csrf', '');

        $nickname = trim($request->param('nickname', ''));

        $password = $request->param('password', '');

        if (!\Minz\CSRF::validate($csrf)) {
            return \Minz\Response::badRequest('persons/new.phtml', [
                'nickname' => $nickname,
                'password' => $password,
                'errors' => [
                    'csrf' => 'Un problème est survenu, veuillez valider le formulaire à nouveau.',
                ],
            ]);
        }

        $person = models\Person::init($nickname, $password);

        $errors = $person->validate();
        if ($errors) {
            return \Minz\Response::badRequest('persons/new.phtml', [
                'nickname' => $nickname,
                'password' => $password,
                'errors' => $errors,
            ]);
        }

        $session_token = $person->renewSession();
        $session_token->save();
        $person->save();

        auth\User::setSessionToken($session_token);

        \Minz\Flash::set('status', 'registered');

        $response = \Minz\Response::redirect('the feed');
        $response->setCookie('hermes_session_token', $session_token->id, [
            'expires' => $session_token->expired_at->getTimestamp(),
            'samesite' => 'Lax',
        ]);
        return $response;
    }
}
