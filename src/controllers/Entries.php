<?php

namespace App\controllers;

use App\auth;
use App\models;
use \Minz\Request;
use \Minz\Response;

/**
 * @author  Marien Fressinaud <dev@marienfressinaud.fr>
 * @license http://www.gnu.org/licenses/agpl-3.0.en.html AGPL
 */
class Entries
{
    public function vote(Request $request): Response
    {
        $user = auth\User::get();
        if (!$user) {
            return Response::redirect('new session');
        }

        $entry_id = $request->paramInteger('id', 0);

        $strength = $request->param('strength', '0');

        $entry = models\Entry::find($entry_id);
        if (!$entry) {
            return Response::redirect('the feed');
        }

        if ($user->hasVotedOn($entry)) {
            return Response::redirect('the feed');
        }

        $remaining_positive_votes = $user->remainingPositiveVotesForToday();

        if ($strength === '+1' && $remaining_positive_votes > 0) {
            $strength = 1;
        } else {
            $strength = 0;
        }

        $vote = models\Vote::init($user, $entry, $strength);
        $vote->save();

        return Response::redirect('the feed');
    }
}
