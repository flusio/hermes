<?php

namespace App\controllers;

use App\auth;
use App\models;
use App\utils;
use \Minz\Request;
use \Minz\Response;

class Home
{
    public function show(Request $request): Response
    {
        $date = utils\Date::getBeginningDay();
        $entries = models\Entry::listForNews($date);
        return Response::ok('home/show.phtml', [
            'status' => \Minz\Flash::pop('status', ''),
            'entries' => $entries,
        ]);
    }

    public function feed(Request $request): Response
    {
        $user = auth\User::get();
        if (!$user) {
            return \Minz\Response::redirect('new session');
        }

        $entries = $user->listFeedEntriesForToday();
        $remaining_positive_votes = $user->remainingPositiveVotesForToday();

        return Response::ok('home/feed.phtml', [
            'status' => \Minz\Flash::pop('status', ''),
            'entries' => $entries,
            'remaining_positive_votes' => $remaining_positive_votes,
        ]);
    }

    public function atom(Request $request): Response
    {
        $date = utils\Date::getBeginningDay();
        $entries = models\Entry::listForNews($date);
        return Response::ok('home/show.atom.xml.php', [
            'entries' => $entries,
            'user_agent' => \Minz\Configuration::$application['user_agent'],
        ]);
    }
}
