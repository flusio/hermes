<?php

namespace App\controllers;

use App\auth;
use App\models;
use \Minz\Request;
use \Minz\Response;

class Sessions
{
    public function new(Request $request): Response
    {
        if (auth\User::get()) {
            return \Minz\Response::redirect('home');
        }

        return \Minz\Response::ok('sessions/new.phtml', [
            'nickname' => '',
            'password' => '',
        ]);
    }

    public function create(Request $request): Response
    {
        if (auth\User::get()) {
            return \Minz\Response::redirect('home');
        }

        $csrf = $request->param('csrf', '');

        $nickname = trim($request->param('nickname', ''));

        $password = $request->param('password', '');

        if (!\Minz\CSRF::validate($csrf)) {
            return \Minz\Response::badRequest('sessions/new.phtml', [
                'nickname' => $nickname,
                'password' => $password,
                'errors' => [
                    'csrf' => 'Un problème est survenu, veuillez valider le formulaire à nouveau.',
                ],
            ]);
        }

        $person = models\Person::findBy(['nickname' => $nickname]);
        if (!$person) {
            return \Minz\Response::badRequest('sessions/new.phtml', [
                'nickname' => $nickname,
                'password' => $password,
                'errors' => [
                    'nickname' => 'Il n’existe aucun compte correspondant à ce pseudonyme.',
                ],
            ]);
        }

        if (!$person->verifyPassword($password)) {
            return \Minz\Response::badRequest('sessions/new.phtml', [
                'nickname' => $nickname,
                'password' => $password,
                'errors' => [
                    'password' => 'Le mot de passe est incorrect, veuillez réessayer.',
                ],
            ]);
        }

        $session_token = $person->renewSession();
        $session_token->save();
        $person->save();

        auth\User::setSessionToken($session_token);

        \Minz\Flash::set('status', 'connected');
        $response = \Minz\Response::redirect('the feed');
        $response->setCookie('hermes_session_token', $session_token->id, [
            'expires' => $session_token->expired_at->getTimestamp(),
            'samesite' => 'Lax',
        ]);
        return $response;
    }

    public function delete(Request $request): Response
    {
        $person = auth\User::get();
        if (!$person) {
            return \Minz\Response::redirect('home');
        }

        $csrf = $request->param('csrf', '');

        if (!\Minz\CSRF::validate($csrf)) {
            return \Minz\Response::redirect('home');
        }

        $person->resetSession();
        $person->save();
        auth\User::reset();

        \Minz\Flash::set('status', 'disconnected');

        $response = \Minz\Response::redirect('home');
        $response->removeCookie('hermes_session_token');
        return $response;
    }
}
