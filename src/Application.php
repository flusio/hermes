<?php

namespace App;

class Application
{
    private \Minz\Router $router;

    private \Minz\Engine $engine;

    public function __construct()
    {
        include_once('utils/view_helpers.php');

        $this->router = new \Minz\Router();

        $this->router->addRoute('get', '/registration', 'Persons#new', 'new person');
        $this->router->addRoute('post', '/registration', 'Persons#create', 'create person');
        $this->router->addRoute('get', '/login', 'Sessions#new', 'new session');
        $this->router->addRoute('post', '/login', 'Sessions#create', 'create session');
        $this->router->addRoute('post', '/logout', 'Sessions#delete', 'delete session');

        $this->router->addRoute('get', '/', 'Home#show', 'home');
        $this->router->addRoute('get', '/the-feed', 'Home#feed', 'the feed');
        $this->router->addRoute('get', '/feed', 'Home#atom', 'web feed');

        $this->router->addRoute('post', '/entries/:id/vote', 'Entries#vote', 'vote for entry');

        $this->engine = new \Minz\Engine($this->router);
        \Minz\Url::setRouter($this->router);

        \Minz\Output\View::$extensions_to_content_types['.atom.xml.php'] = 'application/xml';
    }

    public function run($request)
    {
        $session_token_id = $request->cookie('hermes_session_token');
        if (auth\User::sessionTokenExists() && !$session_token_id) {
            auth\User::reset();
        } elseif (!auth\User::sessionTokenExists() && $session_token_id) {
            auth\User::setSessionTokenId($session_token_id);
        }

        $user = auth\User::get();

        $response = $this->engine->run($request, [
            'controller_namespace' => '\\App\\controllers',
        ]);

        \Minz\Output\View::declareDefaultVariables([
            'environment' => \Minz\Configuration::$environment,
            'brand' => \Minz\Configuration::$application['brand'],
            'csrf' => \Minz\CSRF::generate(),
            'user' => $user,
            'current_page' => '',
        ]);

        return $response;
    }
}
