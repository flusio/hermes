# Hermès, portail d’actualité collaboratif

Hermès est un portail d’actualité dont la ligne éditoriale est gérée de manière collaborative.

Tous les jours à 7h du matin, il génère une liste de liens vers des articles publiés sur différents sites d’actualité.
Les articles sont sélectionnés au fil de la journée précédente par les utilisateurs et utilisatrices de la plateforme.
Les articles sont agrégés depuis les flux Web publiés par les sites d’actualité.

## Utilisation

Une instance d’Hermès est disponible ici : [actu.flus.fr](https://actu.flus.fr).

## Technique

Hermès repose sur

- mon framework maison, [Minz](/lib/Minz) (largement amélioré pour l’occasion) ;
- [SpiderBits](/lib/SpiderBits), une bibliothèque maison pour parser le Web ;
- pas mal de bouts de code issus de [flusio](https://github.com/flusio/flusio).

Il n’existe malheureusement pas de documentation pour installer Hermès.
La documentation de [flusio](https://github.com/flusio/flusio/blob/main/docs/production.md) devrait toutefois constituer une bonne base.

## Prototype

Hermès est un prototype.
Il fonctionne, mais il pourrait être être amélioré.
Quelques évolutions auxquelles j’ai pensé :

- améliorer la vue sur mobile (la navigation dépasse sur les écrans les plus petits) ;
- changer le mode « flux » par un mode « article par article » ;
- permettre de voter par swipe gauche / droite sur mobile ;
- permettre de voter avec les flèches gauche / droite sur PC ;
- changer la contrainte fixe de 20 votes par une barre de votes à recharger en excluant des articles ;
- permettre de se connecter avec plusieurs terminaux ;
- permettre de modifier et supprimer ses infos.

Il n’est pas exclu qu’Hermès soit intégré plus tard sous une forme ou une autre à [flusio](https://github.com/flusio/flusio).
Il n’est toutefois pas prévu de faire évoluer Hermès pour l’instant.

## Licence

Hermès est disponible sous [licence AGPL v3.0 ou plus](/LICENSE.txt).
