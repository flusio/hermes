<?php

$default_db_path = "{$app_path}/data/db.sqlite";

$user_agent = "hermes/dev (https://framagit.org/flusio/hermes)";

return [
    'app_name' => 'App',

    'secret_key' => $dotenv->pop('APP_SECRET_KEY'),

    'url_options' => [
        'host' => $dotenv->pop('APP_HOST'),
        'path' => $dotenv->pop('APP_PATH', '/'),
        'port' => intval($dotenv->pop('APP_PORT', '80')),
    ],

    'application' => [
        'brand' => $dotenv->pop('APP_BRAND', 'Hermès'),
        'user_agent' => $user_agent,
        'cache_path' => $dotenv->pop('APP_CACHE_PATH', $app_path . '/cache'),
        'media_path' => $dotenv->pop('APP_MEDIA_PATH', $app_path . '/public/static/media'),
    ],

    'database' => [
        'dsn' => 'sqlite:' . $dotenv->pop('DB_PATH', $default_db_path),
    ],
];
